package utils

import (
	"fmt"
	"io/ioutil"
	"os"

	"bitbucket.org/seomisw/shisho/pkg/data"
)

var (
	// ErrUnsupportedFormat means the given image format (or file extension) is unsupported.
	ErrUnsupportedFormat = fmt.Errorf("unsupported image format")
)

//ReadFile returns bytes of file
func ReadFile(filename string) ([]byte, error) {

	content, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	return content, nil
}

//ValidExt checks if this is a valid extension
func ValidExt(path string) error {
	var (
		e error
	)
	for i := len(path) - 1; i >= 0 && !os.IsPathSeparator(path[i]); i-- {

		if path[i] == '.' {
			switch path[i:] {
			case data.JPEG:
			case data.JPG:
			case data.PNG:
			case data.WEBP:
			default:
				e = ErrUnsupportedFormat
			}
		}
	}

	return e

}

//Save writes bytes to a given filepath
func Save(filepath string, bytes []byte) error {

	if err := ValidExt(filepath); err != nil {
		return err
	}

	if err := ioutil.WriteFile(filepath, bytes, 0644); err != nil {
		return err
	}
	return nil

}
