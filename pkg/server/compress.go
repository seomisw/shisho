package server

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"

	"bitbucket.org/seomisw/shisho/pkg/data"
	"bitbucket.org/seomisw/shisho/pkg/transformations"
)

func Compress(w http.ResponseWriter, r *http.Request) {
	var (
		opts = data.Opts{
			Compress:         true,
			CompressType:     "zlib5",
			CompressEncoding: "noencoding",
		}
		transBytes []byte
	)
	file, _, err := r.FormFile("image")
	defer file.Close()

	if err != nil {
		json.NewEncoder(w).Encode(data.HttpResp{Status: 500, Description: fmt.Sprintf("%v", err)})
		return
	}

	buf := bytes.NewBuffer(nil)

	if _, err := io.Copy(buf, file); err != nil {
		json.NewEncoder(w).Encode(data.HttpResp{Status: 500, Description: fmt.Sprintf("%v", err)})
		return
	}

	t := transformations.New(opts, "atum.jpg")
	transBytes, err = t.Apply(buf.Bytes())

	if err != nil {
		json.NewEncoder(w).Encode(data.HttpResp{Status: 500, Description: fmt.Sprintf("%v", err)})
		return
	}
	w.Header().Set("Content-Type", "image/jpeg")
	w.Header().Set("Content-Length", strconv.Itoa(len(transBytes)))
	if _, err := w.Write(transBytes); err != nil {
		json.NewEncoder(w).Encode(data.HttpResp{Status: 500, Description: fmt.Sprintf("%v", err)})
	}

	return

}
