package data

import "gopkg.in/gographics/imagick.v2/imagick"

// types of compression
const (
	HUFFMAN = iota
	ZLIB1
	ZLIB2
	ZLIB3
	ZLIB4
	ZLIB5
	ZLIB6
	ZLIB7
	ZLIB8
	ZLIB9
)

// types of encoding/filtering
const (
	NOENCODING = iota
	SUB
	UP
	AVERAGE
	PAETH
	ADAPTATIVE
)

// Image file formats.
const (
	JPEG string = ".jpeg"
	JPG         = ".jpg"
	PNG         = ".png"
	WEBP        = ".webp"
)

var (
	//Filters maps opts filters to imagick format
	Filters = map[string]imagick.FilterType{
		"undefined":      imagick.FILTER_UNDEFINED,
		"point":          imagick.FILTER_POINT,
		"box":            imagick.FILTER_BOX,
		"triangle":       imagick.FILTER_TRIANGLE,
		"hermite":        imagick.FILTER_HERMITE,
		"hanning":        imagick.FILTER_HANNING,
		"hamming":        imagick.FILTER_HAMMING,
		"blackman":       imagick.FILTER_BLACKMAN,
		"gaussian":       imagick.FILTER_GAUSSIAN,
		"quadratic":      imagick.FILTER_QUADRATIC,
		"cubic":          imagick.FILTER_CUBIC,
		"catrom":         imagick.FILTER_CATROM,
		"mitchell":       imagick.FILTER_MITCHELL,
		"jinc":           imagick.FILTER_JINC,
		"sinc":           imagick.FILTER_SINC,
		"sinc_fast":      imagick.FILTER_SINC_FAST,
		"kaise":          imagick.FILTER_KAISER,
		"welsh":          imagick.FILTER_WELSH,
		"parzen":         imagick.FILTER_PARZEN,
		"bohman":         imagick.FILTER_BOHMAN,
		"bartlett":       imagick.FILTER_BARTLETT,
		"lagrange":       imagick.FILTER_LAGRANGE,
		"lanczos":        imagick.FILTER_LANCZOS,
		"lanczos_sharp":  imagick.FILTER_LANCZOS_SHARP,
		"lanczos2":       imagick.FILTER_LANCZOS2,
		"lanczos2_sharp": imagick.FILTER_LANCZOS2_SHARP,
		"robidoux":       imagick.FILTER_ROBIDOUX,
		"robidoux_sharp": imagick.FILTER_ROBIDOUX_SHARP,
		"cosine":         imagick.FILTER_COSINE,
		"spline":         imagick.FILTER_SPLINE,
		"sentinel":       imagick.FILTER_SENTINEL,
		"lanczos_radius": imagick.FILTER_LANCZOS_RADIUS,
	}
	//Compressiontypes maps opts compression type to imagick type
	Compressiontypes = map[string]int{
		"zlib1": ZLIB1,
		"zlib2": ZLIB2,
		"zlib3": ZLIB3,
		"zlib4": ZLIB4,
		"zlib5": ZLIB5,
		"zlib6": ZLIB6,
		"zlib7": ZLIB7,
		"zlib8": ZLIB8,
		"zlib9": ZLIB9,
	}
	//Compressionencoding maps opts compression encoding to imagick encoding
	Compressionencoding = map[string]int{
		"noencoding": NOENCODING,
		"sub":        SUB,
		"up":         UP,
		"paeth":      PAETH,
		"adaptative": ADAPTATIVE,
	}
)

// Resp... the typical struct used to send back json responses
type HttpResp struct {
	Status      int    `json:"status"`
	Description string `json:"description"`
	Body        string `json:"body"`
}

//Opts are options passed to imagick wand
type Opts struct {
	ResizeWidth      uint    `json:"resize_width"`
	ResizeHeight     uint    `json:"resize_height"`
	Fit              bool    `json:"fit"`
	FlipHorizontal   bool    `json:"flip_horizontal"`
	FlipVertical     bool    `json:"flip_vertical"`
	ScaleUp          bool    `json:"scale_up"`
	CropWidth        float64 `json:"crop_width"`
	Crop             bool    `json:"crop"`
	Strip            bool    `json:"strip"`
	Compress         bool    `json:"compress"`
	Filter           string  `json:"filter"`
	Resize           bool    `json:"resize"`
	Blur             bool    `json:"blur"`
	Blurratio        float64 `json:"blurratio"`
	CompressType     string  `json:"compress_type"`
	CompressEncoding string  `json:"compress_encoding"`
}
