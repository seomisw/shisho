package transformations

import (
	"encoding/binary"
	"fmt"

	"bitbucket.org/seomisw/shisho/pkg/data"
	"bitbucket.org/seomisw/shisho/pkg/magick"
)

var (
	err error
	f   int
)

// Transformer contains opts to apply and Magick struct
type Transformer struct {
	Opts data.Opts
	M    *magick.Magick
	path string
}

//New creates and returns a Transformer
func New(opts data.Opts, path string) *Transformer {

	return &Transformer{
		opts,
		magick.NewWand(),
		path,
	}
}

//Apply receives array of bytes and applies transformation options using Wand. Returns transformed bytes.
func (t *Transformer) Apply(b []byte) ([]byte, error) {

	tmp := make([]byte, len(b))
	copy(tmp, b)

	options := t.Opts

	if options == (data.Opts{}) {
		return nil, fmt.Errorf("No transformations to be applied to image")
	}

	if err = t.M.Read(tmp); err != nil {
		t.M.Destroy()
		return nil, err
	}

	if options.Strip {
		if err = t.M.Strip(); err != nil {
			t.M.Destroy()
			return nil, err
		}
	}

	if options.Resize {
		filter, ok := data.Filters[options.Filter]
		if !ok {
			t.M.Destroy()
			return nil, fmt.Errorf("Invalid params for Resize")
		}
		if err = t.M.Resize(options.ResizeWidth, options.ResizeHeight, filter); err != nil {
			t.M.Destroy()
			return nil, err
		}
		f++
	}

	if options.Blur {
		if err = t.M.Blur(options.Blurratio); err != nil {
			t.M.Destroy()
			return nil, err
		}
		f++
	}

	if options.Compress {
		ct, hasType := data.Compressiontypes[options.CompressType]
		ce, hasEncoding := data.Compressionencoding[options.CompressEncoding]
		if !hasEncoding || !hasType {
			t.M.Destroy()
			return nil, fmt.Errorf("Invalid params for Compression")
		}
		if err = t.M.Compress(uint(ct), uint(ce)); err != nil {
			t.M.Destroy()
			return nil, err
		}
		if err = t.M.SetImageFormat("jpg"); err != nil {
			t.M.Destroy()
			return nil, err

		}
		f++
	}
	if f == 0 {
		t.M.Destroy()
		return nil, fmt.Errorf("Unsupported Options")

	}
	transBytes := t.M.GetImageBlob()
	fmt.Println(binary.Size(transBytes))
	return transBytes, nil // kept this because i don-t know if we need the saved file or its bytes. in case we need the bytes it-s easiear to change :)

}
