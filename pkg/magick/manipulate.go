package magick

import "gopkg.in/gographics/imagick.v2/imagick"

var err error

//Magick struct contains the magickwand that performs transformations on image bytes
type Magick struct {
	Wand *imagick.MagickWand
}

//NewWand creates and returns a MAgick struct
func NewWand() *Magick {
	w := imagick.NewMagickWand()
	return &Magick{
		Wand: w,
	}
}

//Read reads blob to wand
func (m *Magick) Read(b []byte) error {
	if err = m.Wand.ReadImageBlob(b); err != nil {
		return err
	}
	return nil
}

//GetImageBlob returns wand bytes
func (m *Magick) GetImageBlob() []byte {
	return m.Wand.GetImageBlob()
}

//Strip strips image of all profiles and comments
func (m *Magick) Strip() error {

	if err = m.Wand.StripImage(); err != nil {
		return nil
	}
	return nil
}

//Resize resizes the image
func (m *Magick) Resize(w, h uint, filter imagick.FilterType) error {

	if err = m.Wand.ResizeImage(w, h, filter, 1); err != nil {
		return err
	}
	return nil

}

//Blur reduces image noise and reduce detail levels
func (m *Magick) Blur(ratio float64) error {

	if err = m.Wand.BlurImage(ratio, 0.0); err != nil {
		return err
	}
	return nil

}

//Compress compresses image
func (m *Magick) Compress(ctype, cencoding uint) error {

	if err = m.Wand.SetImageCompressionQuality(ctype*10 + cencoding); err != nil {
		return err
	}

	return nil

}

//SetImageFormat sets the format of the image
func (m *Magick) SetImageFormat(format string) error {
	if err = m.Wand.SetImageFormat(format); err != nil {
		return err
	}

	return nil

}

//Save writes bytes on wand to path
func (m *Magick) Save(path string) error {
	if err = m.Wand.WriteImage(path); err != nil {
		return err
	}
	return nil
}

func (m *Magick) Destroy() {
	m.Wand.Destroy()
}
