package main

import (
	"log"
	"net/http"

	"bitbucket.org/seomisw/shisho/pkg/server"
)

func main() {

	router := server.NewRouter()
	log.Fatal(http.ListenAndServe(":8080", router))

}
